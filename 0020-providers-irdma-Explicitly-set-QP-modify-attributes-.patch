From 94b468fad35b00bd43d28b9e680a4921baaf75c4 Mon Sep 17 00:00:00 2001
From: Sindhu-Devale <sindhu.devale@intel.com>
Date: Thu, 8 Sep 2022 15:44:12 -0400
Subject: providers/irdma: Explicitly set QP modify attributes for reflush

[ Upstream commit 1ffbbce65e60ee031be70a2bfb6ec319306e4378 ]

irdma issues a reflush via a modify QP to ERROR op to report completions for
WR's posted once the QP is in error state.

However, this reflush modify attributes is incorrectly keyed off the last QP
QP attributes by an application which might or might not be a modify to error.
In the later case, a flush WQE is missed.

Explicitly set the attr.qp_state and attr_mask during a reflush modify
to move the QP to error state once its in error state. Remove ibv_qp
attributes from irdma_uqp struct

Fixes: 14a0fc8 ("rdma-core/irdma: Implement device supported verb APIs")
Signed-off-by: Shiraz Saleem <shiraz.saleem@intel.com>
Signed-off-by: Tatyana Nikolova <tatyana.e.nikolova@intel.com>
Signed-off-by: Sindhu-Devale <sindhu.devale@intel.com>
Signed-off-by: Nicolas Morey-Chaisemartin <nmoreychaisemartin@suse.com>
---
 providers/irdma/umain.h  | 2 --
 providers/irdma/uverbs.c | 9 +++------
 2 files changed, 3 insertions(+), 8 deletions(-)

diff --git a/providers/irdma/umain.h b/providers/irdma/umain.h
index 9e802fd..109e2f1 100644
--- a/providers/irdma/umain.h
+++ b/providers/irdma/umain.h
@@ -88,7 +88,6 @@ struct irdma_ucq {
 
 struct irdma_uqp {
 	struct ibv_qp ibv_qp;
-	struct ibv_qp_attr attr;
 	struct irdma_ucq *send_cq;
 	struct irdma_ucq *recv_cq;
 	struct verbs_mr vmr;
@@ -103,7 +102,6 @@ struct irdma_uqp {
 	struct ibv_recv_wr *pend_rx_wr;
 	struct irdma_qp_uk qp;
 	enum ibv_qp_type qp_type;
-	enum ibv_qp_attr_mask attr_mask;
 	struct irdma_sge *recv_sges;
 };
 
diff --git a/providers/irdma/uverbs.c b/providers/irdma/uverbs.c
index 040b4ec..c2b326d 100644
--- a/providers/irdma/uverbs.c
+++ b/providers/irdma/uverbs.c
@@ -1416,12 +1416,9 @@ int irdma_umodify_qp(struct ibv_qp *qp, struct ibv_qp_attr *attr, int attr_mask)
 	struct irdma_uvcontext *iwctx;
 	struct irdma_uqp *iwuqp;
 
-
 	iwuqp = container_of(qp, struct irdma_uqp, ibv_qp);
 	iwctx = container_of(qp->context, struct irdma_uvcontext,
 			     ibv_ctx.context);
-	iwuqp->attr_mask = attr_mask;
-	memcpy(&iwuqp->attr, attr, sizeof(iwuqp->attr));
 
 	if (iwuqp->qp.qp_caps & IRDMA_PUSH_MODE &&
 	    attr_mask & IBV_QP_STATE && iwctx->uk_attrs.hw_rev > IRDMA_GEN_1) {
@@ -1464,13 +1461,13 @@ static void irdma_issue_flush(struct ibv_qp *qp, bool sq_flush, bool rq_flush)
 {
 	struct ib_uverbs_ex_modify_qp_resp resp = {};
 	struct irdma_umodify_qp cmd_ex = {};
-	struct irdma_uqp *iwuqp;
+	struct ibv_qp_attr attr = {};
 
+	attr.qp_state = IBV_QPS_ERR;
 	cmd_ex.sq_flush = sq_flush;
 	cmd_ex.rq_flush = rq_flush;
-	iwuqp = container_of(qp, struct irdma_uqp, ibv_qp);
 
-	ibv_cmd_modify_qp_ex(qp, &iwuqp->attr, iwuqp->attr_mask,
+	ibv_cmd_modify_qp_ex(qp, &attr, IBV_QP_STATE,
 			     &cmd_ex.ibv_cmd, sizeof(cmd_ex),
 			     &resp, sizeof(resp));
 }
-- 
2.34.1

